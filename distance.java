import java.util.Scanner;


public class distance {

	public static void main(String[] args) {
		
		
		//implemented Scanner method to prompt for user input 
		Scanner input = new Scanner(System.in);
		
		//Prompt User for x and y coordinate for the first point and initiate the values
		//to double variables 
		System.out.println("Please enter your x coordinate for the first point");
		double x = input.nextDouble();
		System.out.println("Please enter your y coordinate for the first point ");
		double y = input.nextDouble();
		
		//Prompt User for x and y coordinate for the first point and initiate the values
	    //to double variables 
		System.out.println("Please enter your x coodinate for the second point");
		double x2 = input.nextDouble();
		System.out.println("Please enter your y coordinate for the second point ");
		double y2 = input.nextDouble();
		//calculate distance between two coordinates 
		//used math.pow to set the difference between x  and y coordinates to the power of two
		
		
		//declare variable distance and calculate the distance between the two points
		//using the distance formula
		
		double distance = Math.sqrt(Math.pow((x2-x), 2)+ Math.pow((y2-y),2));
		System.out.println("The distance between the two points is "+distance);
	
		

	}

}
