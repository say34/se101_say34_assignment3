import java.util.Scanner;

public class Sphere {

	public static void main(String[] args) {
		
		//declare variables for radius, volume, and Surface Area 
		double radius;
		double volume;
		double surfArea;
		
		
		//call Scanner variable for user input 
		Scanner input = new Scanner(System.in);
		
		
		//Prompt user for radius 
		System.out.println("Please enter the radius of the sphere");
		
		//take in user input and set it equal to the radius variable 
		 radius = input.nextDouble();
		
			//Declare volume variable and calculate the volume using the radius provided
			 volume = (4.0/3.0)* Math.PI*Math.pow(radius, 3);
			//Declare surfArea variable and calculate the surface area using the radius provided
			 surfArea = 4*Math.PI*Math.pow(radius, 2);
			
			//Print out the volume and surface area 
			System.out.println("The volume for the following sphere is " +volume);
			System.out.println("The surface area for the following is "+surfArea);
			
		}
		
		

	}


